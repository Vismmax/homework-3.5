import React from "react";
import { Button, Card, Header } from "semantic-ui-react";
import "./CardRoom.css";

export type TypeRoom = {
  id: string;
  name: string;
  countUser: number;
};

type Props = {
  room: TypeRoom;
  joinRoom: (id: string) => void;
};

function CardRoom({ room, joinRoom }: Props): JSX.Element {
  const { name, countUser } = room;
  const join = () => joinRoom(room.id);

  return (
    <Card className="card-room">
      <Card.Content>
        <Card.Meta className="card-users">
          {countUser} users connected
        </Card.Meta>
        <Header as="h2" className="card-title">
          {name}
        </Header>
        <Button size="big" className="card-btn-join" onClick={join}>
          Join
        </Button>
      </Card.Content>
    </Card>
  );
}

export default CardRoom;
