import React from "react";
import { Progress, Icon } from "semantic-ui-react";
import "./UserInfo.css";
import { TypeUser } from "../../models/types";

type Props = {
  user: TypeUser;
  current: boolean;
};

function UserInfo({ user, current }: Props): JSX.Element {
  const { name, isReady, completed } = user;

  return (
    <div className="user-info">
      <div className="user-title">
        <Icon name="circle" color={isReady ? "green" : "red"} />
        <span className={`user-name ${current ? "current-user" : ""}`}>
          {name}
          {current ? " (you)" : ""}
        </span>
      </div>
      <Progress autoSuccess percent={completed} color="grey" />
    </div>
  );
}

export default UserInfo;
