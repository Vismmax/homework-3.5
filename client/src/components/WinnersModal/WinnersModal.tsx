import React from "react";
import { Button, Modal, List } from "semantic-ui-react";

type Winner = {
  name: string;
  completed: number;
};

type Props = {
  winners: {
    show: boolean;
    data: Winner[];
  };
  close: () => void;
};

function WinnersModal({ winners, close }: Props) {
  const { show, data } = winners;
  const list = data.map((e) => (
    <List.Item>
      {e.name} (completed {e.completed}%)
    </List.Item>
  ));
  return (
    <Modal size="tiny" open={show} onClose={close}>
      <Modal.Header>List Winners</Modal.Header>
      <Modal.Content>
        <List ordered>{list}</List>
      </Modal.Content>
      <Modal.Actions>
        <Button onClick={close}>Ok</Button>
      </Modal.Actions>
    </Modal>
  );
}

export default WinnersModal;
