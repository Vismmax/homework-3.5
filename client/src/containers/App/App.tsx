import React, { useEffect, useState } from "react";
import io from "socket.io-client";
import { ToastContainer, toast } from "react-toastify";
import "./App.css";
import { TypeRoom, TextEditor, StatusGame } from "../../models/types";
import { callApi } from "../../helpers/webApi";
import EditorHelper from "../../helpers/editorHelper";
import Login from "../../components/Login/Login";
import { TypeRoom as TypeRoomCard } from "../../components/CardRoom/CardRoom";
import ListRooms from "../../components/ListRooms/ListRooms";
import Room from "../../components/Room/Room";
import WinnersModal from "../../components/WinnersModal/WinnersModal";

const socket = io("");
const editor = EditorHelper.getInstance();
let userName = sessionStorage.getItem("username") ?? "";

function App(): JSX.Element {
  const [rooms, setRooms] = useState([] as TypeRoomCard[]);
  const [activeRoom, setActiveRoom] = useState(null as TypeRoom | null);
  const [statusGame, setStatusGame] = useState({
    isWait: true,
    isTimer: false,
    isGame: false,
  } as StatusGame);
  const [readyTimer, setReadyTimer] = useState(null as number | null);
  const [gameTimer, setGameTimer] = useState(null as number | null);
  const [textEditor, setTextEditor] = useState({ text: "" } as TextEditor);
  const [winners, setWinners] = useState({ show: false, data: [] } as {
    show: boolean;
    data: [];
  });

  const loginUser = (name: string) => {
    socket.emit("LOGIN_USER", name);
  };

  const createRoom = (name: string) => {
    socket.emit("CREATE_ROOM", { name, userName });
  };

  const joinRoom = (id: string) => {
    socket.emit("JOIN_ROOM", { id, userName });
  };

  const leaveRoom = (id: string) => {
    socket.emit("LEAVE_ROOM", { id, userName });
  };

  const toggleReadyUser = () => {
    socket.emit("TOGGLE_READY", { userName });
  };

  const changeCompleted = (completed: number) => {
    socket.emit("CHANGE_COMPLETED", { completed, userName });
  };

  useEffect(() => {
    socket.on("USER_LOGIN", (name: string) => {
      sessionStorage.setItem("username", name);
      userName = name;
    });

    socket.on("ERROR_LOGIN", (error: string) => {
      sessionStorage.removeItem("username");
      showError(error);
    });

    socket.on("UPDATE_ROOMS", (arrayRooms: TypeRoomCard[]) =>
      setRooms(arrayRooms)
    );

    socket.on("ERROR_CREATE_ROOM", (error: string) => {
      showError(error);
    });

    socket.on("JOIN_ROOM_DONE", (room: TypeRoom) => {
      setActiveRoom(room);
    });

    socket.on("ERROR_JOIN_ROOM", (error: string) => {
      showError(error);
    });

    socket.on("LEAVE_ROOM_DONE", (room: TypeRoom) => {
      setActiveRoom(null);
    });

    socket.on("UPDATE_ACTIVE_ROOM", (room: TypeRoom) => {
      setActiveRoom(room);
    });

    socket.on("UPDATE_TIMER", (timer: number) => {
      setReadyTimer(timer);
      if (!statusGame.isTimer) {
        setStatusGame({
          isWait: false,
          isTimer: true,
          isGame: false,
        });
      }
    });

    socket.on("UPDATE_GAME_TIMER", (timer: number) => {
      setGameTimer(timer);
    });

    socket.on("UPDATE_TEXT_GAME", (textId: number) => {
      callApi(`/game/texts/${textId}`, "GET").then(({ text }) => {
        setTextEditor({ textId, text });
        editor.setState({ textId, text });
      });
    });

    socket.on("START_GAME", () => {
      setStatusGame({
        isWait: false,
        isTimer: false,
        isGame: true,
      });

      const { text } = editor.getState();
      editor.setState({
        typedText: "",
        currentChar: text[0],
        targetText: text.slice(1),
      });
      setTextEditor(editor.getState());

      document.addEventListener("keypress", keypressHandler);
    });

    socket.on("STOP_GAME", (winners: []) => {
      document.removeEventListener("keypress", keypressHandler);
      setReadyTimer(null);
      setGameTimer(null);
      setWinners({
        show: true,
        data: winners,
      });
      setStatusGame({
        isWait: true,
        isTimer: false,
        isGame: false,
      });
    });
  }, []);

  const showError = (textError: string) => {
    toast.error(textError);
  };

  const showMessage = ({
    type,
    textMessage,
  }: {
    type: string;
    textMessage: string;
  }) => {
    console.log(type, textMessage);
    alert(textMessage);
  };

  const keypressHandler = (ev: KeyboardEvent) => {
    const { typedText, currentChar, targetText } = editor.getState();
    if (ev.key === currentChar) {
      editor.setState({
        currentChar: (targetText as string)[0],
        typedText: typedText + currentChar,
        targetText: (targetText as string).slice(1),
      });
      setTextEditor(editor.getState());
      changeCompleted(editor.progress());
    }
  };

  const closeWinnersModal = () => {
    setWinners({
      ...winners,
      show: false,
    });
  };

  return (
    <div className="App">
      {!userName && <Login login={loginUser} />}
      {userName && (
        <>
          {!activeRoom && (
            <ListRooms
              rooms={rooms}
              createRoom={createRoom}
              joinRoom={joinRoom}
            />
          )}
          {activeRoom && (
            <Room
              room={activeRoom}
              statusGame={statusGame}
              currentUserName={userName}
              timer={readyTimer}
              gameTimer={gameTimer}
              textEditor={textEditor}
              leaveRoom={leaveRoom}
              toggleReadyUser={toggleReadyUser}
            />
          )}
        </>
      )}
      <WinnersModal winners={winners} close={closeWinnersModal} />
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
    </div>
  );
}

export default App;
