export type TypeUser = {
  id: string;
  name: string;
  isReady: boolean;
  completed: number;
  completedTime?: Date;
};

export type TypeRoom = {
  id: string;
  name: string;
  countUsers: number;
  users: TypeUser[];
  textId?: string;
  text?: string;
  timer?: number;
  timeGame?: number;
};

export type TextEditor = {
  text: string;
  textId?: number;
  typedText?: string;
  targetText?: string;
  currentChar?: string;
};

export type StatusGame = {
  isWait: boolean;
  isTimer: boolean;
  isGame: boolean;
};
