import path from "path";

export const STATIC_PATH = path.join(__dirname, "client/build");
export const HTML_FILES_PATH = path.join(STATIC_PATH, "./");

export const PORT = 3002;
