import { texts } from "../data";

export const getTextById = (id) => {
  return texts[id];
};

export const getRandomTextId = () => {
         return Math.floor(Math.random() * texts.length);
       };
