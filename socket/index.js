import * as config from "./config";
import { getRandomTextId } from "../helpers/dataApi";
import Rooms from "./Rooms";
import Users from "./Users";

const users = new Users();
const rooms = new Rooms();
const timers = new Map();
const maxUsers = config.MAXIMUM_USERS_FOR_ONE_ROOM;

const getCurrentRoomId = (socket) =>
  Object.keys(socket.rooms).find((id) => rooms.getRoomById(id));

const getUserInfo = (socket, userName) => {
  const user = users.getUserByName(userName);
  const roomId = getCurrentRoomId(socket);
  return { user, roomId };
};

export default (io) => {
  io.on("connection", (socket) => {
    socket.emit("UPDATE_ROOMS", rooms.getFilteredRoom(maxUsers));

    socket.on("LOGIN_USER", (name) => {
      if (!name) return;
      if (users.getUserByName(name)) {
        socket.emit("ERROR_LOGIN", "User Exist");
        return;
      }
      socket.emit("USER_LOGIN", name);
      socket.emit("UPDATE_ROOMS", rooms.getFilteredRoom(maxUsers));
      socket.userName = name;
      users.addUser(name);
    });

    socket.on("CREATE_ROOM", ({ name, userName }) => {
      if (rooms.getRoomByName(name)) {
        socket.emit("ERROR_CREATE_ROOM", "Exist room");
        return;
      }
      const room = rooms.addRoom(name);
      socket.join(room.id, () => {
        const user = users.getUserByName(userName);
        rooms.addUserToRoom(room.id, user);
        socket.emit("JOIN_ROOM_DONE", rooms.getRoomById(room.id));
        io.emit("UPDATE_ROOMS", rooms.getFilteredRoom(maxUsers));
      });
    });

    socket.on("JOIN_ROOM", ({ id, userName }) => {
      const joinRoom = rooms.getRoomById(id);
      if (joinRoom.users.length >= maxUsers) {
        socket.emit(
          "ERROR_JOIN_ROOM",
          "Maximum number of participants reached"
        );
        return;
      }
      socket.join(id, () => {
        const user = users.getUserByName(userName);
        const room = rooms.addUserToRoom(id, user);
        socket.emit("JOIN_ROOM_DONE", room);
        io.emit("UPDATE_ROOMS", rooms.getFilteredRoom(maxUsers));
        io.to(id).emit("UPDATE_ACTIVE_ROOM", room);
      });
    });

    socket.on("LEAVE_ROOM", ({ id, userName }) => {
      const { user, roomId } = getUserInfo(socket, userName);
      socket.leave(id);
      const room = rooms.deleteUserFromRoom(id, user);
      socket.emit("LEAVE_ROOM_DONE", room);

      checkRoom(id);
    });

    socket.on("TOGGLE_READY", ({ userName }) => {
      const { user, roomId } = getUserInfo(socket, userName);
      const room = rooms.toggleReadyUser(roomId, user);
      io.to(roomId).emit("UPDATE_ACTIVE_ROOM", room);
      if (room.isReady()) readyGame(roomId);
    });

    socket.on("CHANGE_COMPLETED", ({ completed, userName }) => {
      const { user, roomId } = getUserInfo(socket, userName);
      const room = rooms.changeCompletedUser(roomId, user, completed);
      io.to(roomId).emit("UPDATE_ACTIVE_ROOM", room);
      if (room.isCompleted()) stopGame(roomId);
    });

    socket.on("disconnect", () => {
      const user = users.getUserByName(socket.userName);
      const room = rooms.deleteUserFromAllRoom(user);

      if (room) checkRoom(room.id);
    });

    // const scheckIsReadyRoom = (roomId) => {
    //   const room = rooms.getRoomById(roomId);
    //   if (room.isReady()) readyGame(roomId);
    //   io.to(roomId).emit("UPDATE_ACTIVE_ROOM", room);
    // }

    // const checkIsCompletedRoom = (roomId) => {
    //   const room = rooms.getRoomById(roomId);
    //   if (room.isCompleted()) stopGame(roomId);
    //   io.to(roomId).emit("UPDATE_ACTIVE_ROOM", room);
    // }

    const checkRoom = (roomId) => {
      const room = rooms.getRoomById(roomId);

      if (room.isEmpty()) {
        rooms.deleteEmptyRoom(roomId);
      } else {
        if (room.isCompleted()) {
          stopGame(roomId);
        } else {
          if (room.isReady()) readyGame(roomId);
        }
        io.to(roomId).emit("UPDATE_ACTIVE_ROOM", room);
      }

      io.emit("UPDATE_ROOMS", rooms.getFilteredRoom(maxUsers));
    };

    const readyGame = (roomId) => {
      setReadyTimer(roomId);
      const room = rooms.getRoomById(roomId);
      const textId = getRandomTextId();
      io.to(roomId).emit("UPDATE_ACTIVE_ROOM", room);
      io.to(roomId).emit("UPDATE_TEXT_GAME", textId);
      io.emit("UPDATE_ROOMS", rooms.getFilteredRoom(maxUsers));
    };

    const setReadyTimer = (roomId) => {
      if (timers.has(roomId)) return;
      let time = config.SECONDS_TIMER_BEFORE_START_GAME;
      io.to(roomId).emit("UPDATE_TIMER", time--);
      const readyTimerId = setInterval(() => {
        io.to(roomId).emit("UPDATE_TIMER", time--);
        if (time < 0) {
          clearInterval(readyTimerId);
          timers.delete(roomId);
          startGame(roomId);
        }
      }, 1000);
      timers.set(roomId, readyTimerId);
    };

    const startGame = (roomId) => {
      setGameTimer(roomId);
      io.to(roomId).emit("START_GAME");
    };

    const setGameTimer = (roomId) => {
      if (timers.has(roomId)) return;
      let gameTime = config.SECONDS_FOR_GAME;
      io.to(roomId).emit("UPDATE_GAME_TIMER", gameTime--);
      const gameTimerId = setInterval(() => {
        io.to(roomId).emit("UPDATE_GAME_TIMER", gameTime--);
        if (gameTime < 0) {
          clearInterval(gameTimerId);
          timers.delete(roomId);
          stopGame(roomId);
        }
      }, 1000);
      timers.set(roomId, gameTimerId);
    };

    const stopGame = (roomId) => {
      const room = rooms.getRoomById(roomId);
      clearInterval(timers.get(roomId));
      timers.delete(roomId);
      io.to(roomId).emit("STOP_GAME", room.getWinners());
      room.clearGame();
      io.to(roomId).emit("UPDATE_ACTIVE_ROOM", room);
      io.emit("UPDATE_ROOMS", rooms.getFilteredRoom(maxUsers));
    };
  });
};
